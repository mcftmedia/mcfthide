package im.mcft.mcfthide;

import im.mcft.mcfthide.listeners.Join;
import im.mcft.mcfthide.listeners.PluginEnable;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapAPI;

/**
 * Hides a player on Dynmap when they join.
 * 
 * @author Jon la Cour
 * @version 1.0.2
 */
public class McftHide extends JavaPlugin {
	public static final Logger logger = Logger.getLogger("Minecraft");
	public static DynmapAPI dynmap;
	public static File logFile;
	public static YamlConfiguration logYaml;
	protected static String logpath;

	public void onDisable() {
		getServer().getServicesManager().unregisterAll(this);
		PluginDescriptionFile pdfFile = getDescription();
		String version = pdfFile.getVersion();
		log("Version " + version + " is disabled!", "info");
	}

	public void onEnable() {
		logFile = new File(getDataFolder(), "log.yml");
		checkConfig();
		logYaml = new YamlConfiguration();
		loadConfig();

		Plugin dynmapplugin = getServer().getPluginManager().getPlugin("dynmap");
		dynmap = (DynmapAPI) dynmapplugin;
		if (dynmapplugin == null) {
			log("Unable to hook into Dynmap!", "warning");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		PluginManager pm = getServer().getPluginManager();

		if (dynmapplugin.isEnabled()) {
			log("Hooked into dynmap.", "info");
			pm.registerEvents(new Join (this), this);
		} else {
			log("Unable to hook into Dynmap, we'll wait until it loads.", "info");
			pm.registerEvents(new PluginEnable(this), this);
		}

		PluginDescriptionFile pdfFile = getDescription();
		String version = pdfFile.getVersion();
		log("Version " + version + " enabled", "info");
	}

	/**
	 * Verifies plugin directory exists.
	 * 
	 * @since 1.0.0
	 */
	private void checkConfig() {
		try {
			if (!logFile.exists()) {
				logFile.getParentFile().mkdirs();
				logFile.createNewFile();
			}
		} catch (Exception e) {
			log("Unable to create log file.", "warning");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
	}

	/**
	 * Loads log file.
	 * 
	 * @since 1.0.0
	 */
	private void loadConfig() {
		try {
			logYaml.load(logFile);
		} catch (Exception e) {
			log("Unable to load log file!", "warning");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
	}

	/**
	 * Sends a message to the logger.
	 * 
	 * @param s
	 *            The message to send
	 * @param type
	 *            The level (info, warning, severe)
	 * @since 1.0.0
	 */
	public static void log(final String s, final String type) {
		String message = "[McftHide] " + s;
		String t = type.toLowerCase();
		if (t != null) {
			boolean info = t.equals("info");
			boolean warning = t.equals("warning");
			boolean severe = t.equals("severe");
			if (info) {
				logger.info(message);
			} else if (warning) {
				logger.warning(message);
			} else if (severe) {
				logger.severe(message);
			} else {
				logger.info(message);
			}
		}
	}
}