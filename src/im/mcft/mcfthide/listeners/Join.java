package im.mcft.mcfthide.listeners;

import static im.mcft.mcfthide.McftHide.log;
import im.mcft.mcfthide.McftHide;

import java.io.File;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.dynmap.DynmapAPI;

public class Join implements Listener {
	protected McftHide plugin;
	protected DynmapAPI dynmap;
	protected YamlConfiguration logYaml;
	protected File logFile;

	public Join(McftHide instance) {
		plugin = instance;
		logYaml = McftHide.logYaml;
		logFile = McftHide.logFile;
		dynmap = McftHide.dynmap;
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent event) {
		String name = event.getPlayer().getName();
		if (logYaml.getBoolean(name, false)) {
			return;
		} else {
			dynmap.assertPlayerInvisibility(event.getPlayer(), true, plugin);
			logYaml.set(name, true);
			try {
				logYaml.save(logFile);
			} catch (Exception e) {
				log("Error saving name to log file.", "info");
			}
		}
	}
}
