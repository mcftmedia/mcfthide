package im.mcft.mcfthide.listeners;

import im.mcft.mcfthide.McftHide;

import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.PluginManager;

public class PluginEnable implements Listener {
	protected McftHide plugin;

	public PluginEnable(McftHide instance) {
		plugin = instance;
	}
	
	public void onPluginEnable(PluginEnableEvent event) {
		String name = event.getPlugin().getDescription().getName();
		if (name.equals("dynmap")) {
			PluginManager pm = plugin.getServer().getPluginManager();
			pm.registerEvents(new Join(plugin), plugin);
		}
	}
}
