# McftHide
_Production Build v1.0.2_

A Bukkit server plugin for dynmap that hides new players when they first join your server.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

### [License](https://github.com/laCour/McftHide/blob/master/LICENSE.md)